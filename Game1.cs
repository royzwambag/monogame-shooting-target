﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ShootingGallery
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private Texture2D targetSprite;
        private Texture2D crosshairsSprite;
        private Texture2D backgroundSprite;
        private SpriteFont gameFont;

        private Vector2 targetPosition = new Vector2(300, 300);
        private const int targetRadius = 45;

        private MouseState _mouseState;
        private bool lmbReleased = true;

        private int score = 0;
        private double timer = 10;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            targetSprite = Content.Load<Texture2D>("target");
            crosshairsSprite = Content.Load<Texture2D>("crosshairs");
            backgroundSprite = Content.Load<Texture2D>("sky");
            gameFont = Content.Load<SpriteFont>("galleryFont");
            Mouse.SetCursor(MouseCursor.FromTexture2D(crosshairsSprite, 25, 25));
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (timer > 0)
            {
                timer -= gameTime.ElapsedGameTime.TotalSeconds;

                _mouseState = Mouse.GetState();
                if (mouseClicked(_mouseState))
                {
                    float mouseTargetDistance = Vector2.Distance(targetPosition, _mouseState.Position.ToVector2());
                    if (mouseTargetDistance < targetRadius)
                    {
                        score++;

                        Random random = new Random();
                        targetPosition.X = random.Next(45, _graphics.PreferredBackBufferWidth - 45);
                        targetPosition.Y = random.Next(45, _graphics.PreferredBackBufferHeight - 45);
                    }

                    lmbReleased = false;
                }

                if (_mouseState.LeftButton == ButtonState.Released)
                    lmbReleased = true;
            }
            
            if (timer < 0)
                timer = 0;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            _spriteBatch.Draw(backgroundSprite, new Vector2(0, 0), Color.White);
            _spriteBatch.DrawString(gameFont, $"score: {score.ToString()}", new Vector2(3, 3), Color.DarkGoldenrod);
            _spriteBatch.DrawString(gameFont, $"time: {Math.Ceiling(timer).ToString()}", new Vector2(3, 40), Color.DarkGoldenrod);

            if (timer > 0)
            {
                _spriteBatch.Draw(targetSprite,
                    new Vector2(targetPosition.X - targetRadius, targetPosition.Y - targetRadius), Color.White);
            }

            _spriteBatch.End();

            base.Draw(gameTime);
        }

        private bool mouseClicked(MouseState _mouseState)
        {
            return _mouseState.LeftButton == ButtonState.Pressed && lmbReleased == true;
        }
    }
}